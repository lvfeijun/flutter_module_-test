//
//  AppDelegate.h
//  flutterTest
//
//  Created by lvfeijun on 2022/3/2.
//

#import <UIKit/UIKit.h>
#import <Flutter/Flutter.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,FlutterAppLifeCycleProvider>


@end

