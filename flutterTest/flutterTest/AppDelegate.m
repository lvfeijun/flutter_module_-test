//
//  AppDelegate.m
//  flutterTest
//
//  Created by lvfeijun on 2022/3/2.
//

#import "AppDelegate.h"

#import <FlutterPluginRegistrant/GeneratedPluginRegistrant.h>
#import "MyFlutterBoostDelegate.h"
#import <flutter_boost/FlutterBoost.h>
#import "FlutterRoute.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
{
  FlutterPluginAppLifeCycleDelegate *_lifeCycleDelegate;
}
- (instancetype)init {
    if (self = [super init]) {
        _lifeCycleDelegate = [[FlutterPluginAppLifeCycleDelegate alloc] init];
    }
    return self;
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [FlutterRoute setUpFlutterWithApplication:application navigationController:tabBarRootViewController.selectedViewController.navigationController];
    return [_lifeCycleDelegate application:application didFinishLaunchingWithOptions:launchOptions];
}

- (void)addApplicationLifeCycleDelegate:(nonnull NSObject<FlutterApplicationLifeCycleDelegate> *)delegate {
    
}

#pragma mark - UISceneSession lifecycle


- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
}


- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions {
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
}


@end
