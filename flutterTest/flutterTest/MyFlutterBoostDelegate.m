//
//  MyFlutterBoostDelegate.m
//  iOSProject
//
//  Created by lvfeijun on 2022/5/5.
//

#import <Foundation/Foundation.h>
#import "MyFlutterBoostDelegate.h"
#import "HQTabBarRootViewController.h"
#import <flutter_boost/FlutterBoost.h>
#import "FlutterRoute.h"

@interface MyFlutterBoostDelegate()

@property (nonatomic, strong) FBFlutterViewContainer *flutterV;

@end

@implementation MyFlutterBoostDelegate

+ (instancetype)shareMyFlutterBoostDelegate
{
    static MyFlutterBoostDelegate *router = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        router = [[MyFlutterBoostDelegate alloc] init];
    });
    return router;
}
- (void) pushNativeRoute:(NSString *) pageName arguments:(NSDictionary *) arguments
{
    NSLog(@"pushNativeRoute");
    BOOL animated = [arguments[@"animated"] boolValue];
    BOOL present= [arguments[@"present"] boolValue];
    [[HQTabBarRootViewController currentWidowsViewController].navigationController setNavigationBarHidden:YES];
    if (present) {
//        [[HQTabBarRootViewController currentWidowsViewController] dismissViewControllerAnimated:animated completion:nil];
        [self.navigationController dismissViewControllerAnimated:animated completion:nil];
    } else {
//        [[HQTabBarRootViewController currentWidowsViewController].navigationController popViewControllerAnimated:animated];
        [self.navigationController popViewControllerAnimated:animated];
    }
}

- (UINavigationController *)navigationController
{
    if (!_navigationController) {
        _navigationController = [HQTabBarRootViewController currentWidowsViewController].navigationController;
    }
    return _navigationController;
}

- (FBFlutterViewContainer *)flutterV
{
    if (!_flutterV) {
        _flutterV = FBFlutterViewContainer.new;
    }
    return _flutterV;
}

- (void)pushFlutterRoute:(FlutterBoostRouteOptions *)options
{
    FBFlutterViewContainer *vc = self.flutterV;
    [MyFlutterBoostDelegate shareMyFlutterBoostDelegate].flutterV = self.flutterV;
    //防止flutter页面被释放,重新将flutter页面加到栈中
    [vc setName:options.pageName uniqueId:options.uniqueId params:options.arguments opaque:options.opaque];
    
    //是否伴随动画
    BOOL animated = [options.arguments[@"animated"] boolValue];
    //是否是present的方式打开,如果要push的页面是透明的，那么也要以present形式打开
    BOOL present = [options.arguments[@"present"] boolValue] || !options.opaque;
//    [self.navigationController setNavigationBarHidden:YES];
    if (present) {
        
//        [[HQTabBarRootViewController currentWidowsViewController].navigationController presentViewController:vc animated:animated completion:^{
//            options.completion(YES);
//        }];
        [self.navigationController presentViewController:vc animated:animated completion:^{
            options.completion(YES);
        }];
    } else {
//        [[HQTabBarRootViewController currentWidowsViewController].navigationController pushViewController:vc animated:animated];
        [self.navigationController pushViewController:vc animated:animated];
        options.completion(YES);
    }
}

- (void) popRoute:(FlutterBoostRouteOptions *)options
{
    //拿到当前vc
//    FBFlutterViewContainer *vc = (id)self.navigationController.presentedViewController;
//    if (![vc isKindOfClass:FBFlutterViewContainer.class]) {
//        vc = [HQTabBarRootViewController currentWidowsViewController];
//    }
    FBFlutterViewContainer *vc = self.flutterV;
//    @weakify(vc);
    //是否伴随动画,默认是true
    BOOL animated = true;
    NSNumber *animatedValue = options.arguments[@"animated"];
    if (animatedValue) {
        animated = [animatedValue boolValue];
    }

    BOOL ispresent =
//    self.navigationController.viewControllers.count<2 ||
    ([vc isKindOfClass:FBFlutterViewContainer.class] && [vc.uniqueIDString isEqual: options.uniqueId]);
    @weakify(self)
    //present的情况，走dismiss逻辑
    if (ispresent) {
        
        //这里分为两种情况，由于UIModalPresentationOverFullScreen下，生命周期显示会有问题
        //所以需要手动调用的场景，从而使下面底部的vc调用viewAppear相关逻辑
        if (vc.modalPresentationStyle == UIModalPresentationOverFullScreen) {
            
            //这里手动beginAppearanceTransition触发页面生命周期
            [self.navigationController.topViewController beginAppearanceTransition:YES animated:NO];
            [self.navigationController setNavigationBarHidden:YES];


            [vc dismissViewControllerAnimated:animated completion:^{
                [self.navigationController.topViewController endAppearanceTransition];
//                @strongify(vc);
                @strongify(self)
                [self doDeallocFlutterView];
            }];
        } else {
            //正常场景，直接dismiss
            [vc dismissViewControllerAnimated:animated completion:^{
//                @strongify(vc)
                @strongify(self)
                [self doDeallocFlutterView];
            }];
        }
    } else {
        //否则走pop逻辑
        [self.navigationController popViewControllerAnimated:YES];
        [self doDeallocFlutterView];
    }
    
    options.completion(YES);
}

- (void)doDeallocFlutterView
{
    for (FBVoidCallback loginBlock in [FlutterRoute getFBVoidCallbackArray]) {
        loginBlock();
    }
    
    [FlutterRoute resetFBVoidCallbackArray];
    if (self.flutterV) {
        [self.flutterV notifyWillDealloc];
        self.flutterV = nil;
    }
}

+ (void)doDeallocFlutterView
{
    [[MyFlutterBoostDelegate shareMyFlutterBoostDelegate] doDeallocFlutterView];
}

+ (UIViewController *)getCurrentVC
{
    return [MyFlutterBoostDelegate shareMyFlutterBoostDelegate].flutterV;
}

@end
