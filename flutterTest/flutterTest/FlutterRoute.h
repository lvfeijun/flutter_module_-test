//
//  FlutterRoute.h
//  hqedu24olapp
//
//  Created by lvfeijun on 2022/9/2.
//  Copyright © 2022 edu24ol. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FlutterRoute : NSObject

+ (NSArray *)getFBVoidCallbackArray;
+ (void)resetFBVoidCallbackArray;

+ (void)setUpFlutterWithApplication:(UIApplication *)application navigationController:(UINavigationController *)navigationController;

+ (void)goToLogin;

+ (void)btn_click:(NSString *)route;

@end

NS_ASSUME_NONNULL_END
