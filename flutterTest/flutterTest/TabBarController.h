//
//  TabBarController.h
//  flutterTest
//
//  Created by lvfeijun on 2022/3/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TabBarController : UITabBarController

@end

NS_ASSUME_NONNULL_END
