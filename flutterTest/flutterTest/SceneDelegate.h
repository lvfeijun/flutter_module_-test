//
//  SceneDelegate.h
//  flutterTest
//
//  Created by lvfeijun on 2022/3/2.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

