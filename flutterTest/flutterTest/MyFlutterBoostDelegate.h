//
//  MyFlutterBoostDelegate.h
//  iOSProject
//
//  Created by lvfeijun on 2022/5/5.
//

#import <flutter_boost/FlutterBoost.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyFlutterBoostDelegate : NSObject<FlutterBoostDelegate>

@property (nonatomic,strong) UINavigationController *navigationController;

+ (void)doDeallocFlutterView;

+ (UIViewController *)getCurrentVC;

@end

NS_ASSUME_NONNULL_END
