#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import argparse
import subprocess

def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('action', choices=['build', 'release'], default='build')
    parser.add_argument('--platform', choices=['ios', 'android', 'windows'])
    parser.add_argument('--version')
    parser.add_argument('--output_dir')
    parser.add_argument('--macro_arg')

    return parser.parse_args()

def fetch_submodule(is_develop):
    cmd = 'git submodule update --init'
    if is_develop:
        cmd += ' --remote'
    subprocess.call(cmd.split(' '))

def main():
    print((sys.version_info))
    cfg = parse_args()
    fetch_submodule(cfg.action == 'build')
    from ci_build import ios, android, windows, sync_version
    sync_version.generate_version_header(version=cfg.version)
    locals()[cfg.platform].run(cfg)


if __name__ == '__main__':
    main()
