#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os
import glob
import shutil
import json
import zipfile
import platform
import xml.etree.cElementTree as ET
if sys.version > '3':
   import urllib.request as urllib2
else:
   import urllib2
from . import utils,sync_version

try:
    NDK_ROOT = os.environ['ANDROID_NDK_HOME']
    ANDROID_HOME = os.environ['ANDROID_HOME']
except KeyError as identifier:
    NDK_ROOT = ''
    ANDROID_HOME = ''

#python ci-build.py build --platform android --version 1.0.0-SNAPSHOT --output_dir /Users/hqwx/new_sdk/build  --macro_arg local_build
CMAKE_PATH = ANDROID_HOME + '/cmake/3.6.4111459/bin/cmake'
BUILD_OUT_PATH = 'build/Android'
ANDROID_BUILD_CMD = '%s %s -DCMAKE_INSTALL_PREFIX=%s -DANDROID_ABI="%s" -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=%s/build/cmake/android.toolchain.cmake -DANDROID_TOOLCHAIN=clang -DANDROID_NDK=%s -DANDROID_PLATFORM=android-14 -DANDROID_STL="c++_shared" && %s --build . --config Release -- -j8 && make install'

DEPENDENCY_PATH = os.path.join(utils.root_path(), 'and_aar')

def system_architecture_is64():
    return platform.machine().endswith('64')
#NDK strip
ANDROID_STRIP_FILE = {
        'armeabi': NDK_ROOT + '/toolchains/arm-linux-androideabi-4.9/prebuilt/%s/bin/arm-linux-androideabi-strip',
        'armeabi-v7a': NDK_ROOT + '/toolchains/arm-linux-androideabi-4.9/prebuilt/%s/bin/arm-linux-androideabi-strip',
        'x86': NDK_ROOT + '/toolchains/x86-4.9/prebuilt/%s/bin/i686-linux-android-strip',
        'arm64-v8a': NDK_ROOT + '/toolchains/aarch64-linux-android-4.9/prebuilt/%s/bin/aarch64-linux-android-strip',
        'x86_64': NDK_ROOT + '/toolchains/x86_64-4.9/prebuilt/%s/bin/x86_64-linux-android-strip',
         }

ANDROID_STL_FILE = {
        'armeabi': NDK_ROOT + '/sources/cxx-stl/llvm-libc++/libs/armeabi/libc++_shared.so',
        'armeabi-v7a': NDK_ROOT + '/sources/cxx-stl/llvm-libc++/libs/armeabi-v7a/libc++_shared.so',
        'x86': NDK_ROOT + '/sources/cxx-stl/llvm-libc++/libs/x86/libc++_shared.so',
        'arm64-v8a': NDK_ROOT + '/sources/cxx-stl/llvm-libc++/libs/arm64-v8a/libc++_shared.so',
        'x86_64': NDK_ROOT + '/sources/cxx-stl/llvm-libc++/libs/x86_64/libc++_shared.so',
        }

def get_android_strip_cmd(arch):

    system_str = platform.system().lower()
    if (system_architecture_is64()):
        system_str = system_str + '-x86_64'
    else:
        pass

    strip_cmd = ANDROID_STRIP_FILE[arch] %(system_str)
    print('Android strip cmd:%s' %(strip_cmd))
    return strip_cmd

def project_info():
    project_file = os.path.join(utils.root_path(), 'android/project.json')
    return load_json(project_file)

def load_json(path):
    with open(path) as json_file:
        return json.load(json_file)

def get_download_url(group, artifact, version, abi, file_name):
    url = 'http://repo.duowan.com:8181/nexus/content/groups/public'
    url = url+'/'+group+'/'+artifact+'/'+version
    url = url + '/' + file_name
    return url

def dependencies_info_mapper():
    info_mapper_path = os.path.join(utils.root_path(), 'ci_build/android_ver_mapper.json')
    return utils.load_json(info_mapper_path)

def download_dependencies_abi(project_info,arch):
    info_mapper = dependencies_info_mapper()
    name_mapper = project_info['name_mapper']
    for info in project_info['dependencies']:
        group = info['group']
        artifact = info['artifact']
        version = info['version']
        if info_mapper.get(artifact) is not None:
            mapper_ver = info_mapper[artifact]["versions"][version]
            if mapper_ver is not None:
                version = mapper_ver
        
        abi = arch
        file_type = info['type']

        is_snapshot = version.endswith("-SNAPSHOT")
        if is_snapshot :
            file_name = "maven-metadata.xml"
        else :
            file_name = artifact + '-' + version + '-' + abi + '.' + file_type

        url = get_download_url(group,artifact,version,abi,file_name)
        print('====== download url =='+url+"======")
        name = name_mapper.get(artifact)
        if name is not None:
            artifact = name
        target_dir = os.path.join(DEPENDENCY_PATH,artifact+"/"+abi)
        if not os.path.exists(target_dir):
            os.makedirs(target_dir)

        ######自定义文件夹
        folder = info.get('folder')
        if folder is not None:
            target_dir = os.path.join(target_dir,folder)
            if not os.path.exists(target_dir):
                os.mkdir(target_dir)
        if file_type == 'so' or file_type == "a":
            file_name = 'lib'+artifact+"."+file_type

        target_path = os.path.join(target_dir,file_name)
        download(url,target_path)

        if is_snapshot : 
            tree = ET.parse(os.path.join(target_dir,"maven-metadata.xml"))
            root = tree.getroot()
            for versioning in root.findall("versioning"):
                for snapshot in versioning.findall("snapshot") :
                    timestamp = snapshot.find("timestamp").text
                    buildNumber = snapshot.find("buildNumber").text
            file_name = artifact + '-' + version.replace("-SNAPSHOT","") + '-' + timestamp + "-" + buildNumber + "-" + abi + '.' + file_type
            url = get_download_url(group,artifact,version,abi,file_name)
            target_path = os.path.join(target_dir,file_name)
            print('====== download url =='+url+"======")
            download(url,target_path)

        if file_type == 'har':
            unzipfile(target_path,target_dir)
            os.remove(target_path)

    return True
    
def download_dependencies(project_info,archs):
    for arch in archs:
        if not download_dependencies_abi(project_info,arch):
            return False
    return True     

def download(url, dst):
    ret = None
    try:
        ret = urllib2.urlopen(url,timeout=30)
    except urllib2.HTTPError as e:
        print('====download HTTPError=====')

    if ret is not None:
        data = ret.read()
        with open(dst, 'wb') as w:
            w.write(data)
        print('download success [%s]'%url)

def unzipfile(file_path, dest_dir):
    if not os.path.exists(dest_dir):
        os.mkdir(dest_dir)

    zf = zipfile.ZipFile(file_path)
    # try:
    zf.extractall(path=dest_dir)
    # except RuntimeError as e:
    #    print(e)
    zf.close()

def build_android_abi(local_build,project,arch):
    utils.clean(BUILD_OUT_PATH)
    os.chdir(BUILD_OUT_PATH)
    project_path = utils.root_path()
    build_path = os.path.join(project_path, BUILD_OUT_PATH+"/"+arch)
    build_cmd = ANDROID_BUILD_CMD %(CMAKE_PATH,project_path,build_path,arch, NDK_ROOT, NDK_ROOT,CMAKE_PATH)
    print(("build cmd:" + build_cmd))
    ret = os.system(build_cmd)
    if ret != 0:
        print('==================build fail========================')
        return False

    #ndk srtip prebuilt 生成正式可用 so
    # lib_path = arch+"/"+project['name'] + '/libs'
    out_lib_path = os.path.join(build_path, project['name'] + '/libs')
    strip_cmd = get_android_strip_cmd(arch)
    for f in glob.glob('%s/*.so' %(out_lib_path)):
        os.system('%s %s' %(strip_cmd, f))

    android_path = os.path.join(project_path,'android')
    print("android_path:" + android_path+",out_lib_path:"+out_lib_path)
    if project['copy_lib'] == 1:
        copy_file(out_lib_path,android_path,arch)
    elif project['copy_lib'] == 2:
        shutil.copy(ANDROID_STL_FILE[arch], out_lib_path)
        copy_file2(out_lib_path,android_path,arch)

    if local_build:
        project_name = project['name']
        if project_name == 'hqwxbase':
            update_dependencies(build_path,'hqwxsignalling',project_name,arch)
            update_dependencies(build_path,'hqwxclassing',project_name,arch)
            update_dependencies(build_path,'hqwxclassroom',project_name,arch)
            update_dependencies(build_path,'hqwxwhiteboard',project_name,arch)
        elif project_name == 'hqwxsignalling':
            update_dependencies(build_path,'hqwxclassroom',project_name,arch)
            update_dependencies(build_path,'hqwxclassing',project_name,arch)
        elif project_name == 'hqwxclassroom':
            update_dependencies(build_path,'hqwxclassing',project_name,arch)
            update_dependencies(build_path,'hqwxwhiteboard',project_name,arch)
            update_dependencies(build_path,'hqwxinteractive',project_name,arch)
        elif project_name == 'hqwxinteractive':
            update_dependencies(build_path,'hqwxclassing',project_name,arch)
        elif project_name == 'hqwxmetrics':
            update_dependencies(build_path,'hqwxclassing',project_name,arch)
            update_dependencies(build_path,'hqwxsignalling',project_name,arch)
            update_dependencies(build_path,'hqwxclassroom',project_name,arch)
            update_dependencies(build_path,'hqwxwhiteboard',project_name,arch)
            update_dependencies(build_path,'hqwxinteractive',project_name,arch)

    return True

def build_android(local_build,project,archs):
    for arch in archs:
        if not build_android_abi(local_build, project,arch):
            return False
    if local_build:
        return True
    project_path = utils.root_path()
    android_path = os.path.join(project_path,'android')
    os.chdir(os.path.join(android_path,'gradle_build'))
    #bash gradlew clean assembleRelease bash gradlew clean publish
    ret = os.system('bash gradlew clean publish')
    if ret:
        print('==================publish fail========================'+android_path)
        return False
    print('==================publish success========================'+android_path)    
    return True        

#本地构建 更新到引用的库
def update_dependencies(build_path,module_name,project_name,arch):
    build_path = build_path+"/"+project_name
    depend_path = os.path.dirname(utils.root_path())+'/'+module_name+'/depends/'+project_name+"/"+arch
    if os.path.exists(depend_path):
        del_file(depend_path)
        shutil.rmtree(depend_path,True)
    os.makedirs(depend_path)
    shutil.copytree(build_path+"/include",depend_path+"/include")
    shutil.copytree(build_path+"/libs",depend_path+"/libs")
    print("===== update "+module_name+" dependencies ===="+build_path)

def del_file(path):
    for i in os.listdir(path):
        path_file = os.path.join(path,i)
        if os.path.isfile(path_file):
            os.remove(path_file)
        else:
            del_file(path_file)

def copy_file(out_lib_path,android_path,arch):
    target_path = os.path.join(android_path,"libs/"+arch)
    if os.path.exists(target_path):
        shutil.rmtree(target_path)
    os.makedirs(target_path)
    flist = os.listdir(out_lib_path)
    for file_name in flist:
        shutil.copyfile(os.path.join(out_lib_path,file_name),os.path.join(target_path,file_name))

    print('========coy_file========'+target_path)
    return target_path

def copy_file2(out_lib_path,android_path,arch):
    target_path = copy_file(out_lib_path,android_path,arch)
    shutil.copyfile(utils.root_path()+"/depends/hqwxsignalling/"+arch+"/libs/libhqwxsignalling.so",os.path.join(target_path,'libhqwxsignalling.so'))
    shutil.copyfile(utils.root_path()+"/depends/hqwxbase/"+arch+"/libs/libhqwxbase.so",os.path.join(target_path,'libhqwxbase.so'))
    #shutil.copyfile(utils.root_path()+"/depends/hqwxclassroom/libs/libhqwxclassroom.so",os.path.join(target_path,'libhqwxclassroom.so'))
    #shutil.copyfile(utils.root_path()+"/depends/hqwxinteractive/libs/libhqwxinteractive.so",os.path.join(target_path,'libhqwxinteractive.so'))
    print('========coy_file2========')

def version_header_content(project_info, file_name):
    _name = project_info.get('name')
    _version_name = _name.upper()
    _version = project_info.get('version', '1.0.0-dev')
    _version = dependencies_info_mapper().get(_name).get('versions').get(_version)
    _build_num = os.environ.get('BUILD_NUMBER', 1)
    _commit_id = utils.get_git_revision_hash(project_path=utils.root_path(), is_short=True)
    return '''#ifndef {file_name}_h
#define {file_name}_h
#define {version_name}_VERSION "{version}"
#define {version_name}_BUILD_NUM {build_num}
#define {version_name}_COMMIT_ID "{commit_id}"
#endif /* {file_name}_h */
'''.format(file_name=file_name, version_name=_version_name, version=_version, build_num=_build_num, commit_id=_commit_id)

def flutter_build_sh():
    print('ci-build-and')
    os.chdir(utils.root_path())
    os.system('ls')
    return os.system('sh ci-build-and.sh');

def flutter_build_aar():
    print('flutter_build_aar')
    os.chdir(utils.root_path())
    return os.system('flutter build aar')

def flutter_build_apk():
    print('flutter_build_apk')
    os.chdir(utils.root_path())
    return os.system('flutter build apk')

def generate_zip(output_dir, project_name):
    product_dir = os.path.join(output_dir, project_name)
    shutil.make_archive(product_dir, 'zip', DEPENDENCY_PATH)

def run(cfg):
    print('--------android---------')
    print(cfg)
    project = utils.project_info()


    if not os.path.exists(cfg.output_dir):
        os.makedirs(cfg.output_dir)

    flutter_build_sh()

    project_name = project['name']
    print(project_name)
    generate_zip(cfg.output_dir, project_name)



    # local_bool = cfg.macro_arg == "local_build"

    # project = project_info()
    # file_name = '{}_version'.format(project.get('name'))
    # content = version_header_content(project, file_name)
    # file_path = os.path.join(utils.root_path(), 'src/{}.h'.format(file_name))
    # utils.write_content(content, file_path)
    # archs = set(["arm64-v8a","armeabi-v7a"])
    # if local_bool :
    #     if os.path.exists(DEPENDENCY_PATH):
    #         build_android(local_bool,project,archs)
    #     else:
    #         project = project_info()
    #         print(project)
    #         os.mkdir(DEPENDENCY_PATH)
    #         if download_dependencies(project,archs) and build_android(local_bool,project,archs):
    #             print('==================output========================')
    #             exit(0)
    #         exit(1)
    # else:
    #     if os.path.exists(DEPENDENCY_PATH):
    #         shutil.rmtree(DEPENDENCY_PATH)
    #     os.mkdir(DEPENDENCY_PATH)
    #     print(project)
    #     if download_dependencies(project,archs) and build_android(local_bool,project,archs):
    #         print('==================output========================')
    #         exit(0)
    #     exit(1)
