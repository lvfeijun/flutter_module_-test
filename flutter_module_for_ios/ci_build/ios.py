#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import glob
import shutil
from . import utils


BUILD_OUT_PATH = 'build/iOS'

POD_PROJECT_PATH = os.path.join(utils.root_path(), '.ios')
DEPENDENCY_PATH = os.path.join(utils.root_path(), 'depends')
FRAMEWORK_PATH = os.path.join(utils.root_path(), 'ios_frameworks')

GEN_IOS_OS_PROJ = 'cmake ../.. -G Xcode -DCMAKE_TOOLCHAIN_FILE=../../ci_build/cmake/ios.toolchain.cmake -DIOS_PLATFORM=OS -DIOS_ARCH="armv7;arm64" -DENABLE_ARC=0 -DENABLE_BITCODE=0 -DENABLE_VISIBILITY=1'

def install_path(project_name):
    return '{}/{}'.format(BUILD_OUT_PATH, project_name)

def run_make(is_simulator):
    platform = 'SIMULATOR' if is_simulator else 'OS'
    archs = '"x86_64"' if is_simulator else '"armv7;arm64"'

    project_path = utils.root_path()
    build_path = os.path.join(project_path, BUILD_OUT_PATH)
    params = {
        'CMAKE_INSTALL_PREFIX': build_path,
        'CMAKE_BUILD_TYPE': 'Release',
        'CMAKE_TOOLCHAIN_FILE': os.path.join(project_path, 'ci_build/cmake/ios.toolchain.cmake'),
        'IOS_DEPLOYMENT_TARGET': '9.0',
        'IOS_PLATFORM': platform,
        'IOS_ARCH': archs,
        'ENABLE_ARC': '1',
        'ENABLE_BITCODE': '0',
        'ENABLE_VISIBLITY': '1'
    }
    cmd = ' '.join(['-D{}={}'.format(k, v) for (k, v) in params.items()])
    cmd = 'cmake {} {} && make -j8 && make install'.format(project_path, cmd)
    print(cmd)
    utils.clean(build_path)
    os.chdir(build_path)
    ret = os.system(cmd)
    os.chdir(project_path)
    return ret == 0

def build(project_name):
    ret = run_make(False)
    if not ret:
        print('!!!!!!!!!!!build os fail!!!!!!!!!!!!!!!')
        return False

    lib_path = install_path(project_name) + '/libs'
    libtool_os_dst_lib = lib_path + '/os'
    if not utils.libtool_libs(glob.glob(lib_path + '/*.a'), libtool_os_dst_lib):
        return False

    ret = run_make(True)
    if not ret:
        print('!!!!!!!!!!!build simulator fail!!!!!!!!!!!!!!!')
        return False

    libtool_simulator_dst_lib = lib_path + '/simulator'
    if not utils.libtool_libs(glob.glob(lib_path + '/*.a'), libtool_simulator_dst_lib):
        return False

    lipo_src_libs = []
    lipo_src_libs.append(libtool_os_dst_lib)
    lipo_src_libs.append(libtool_simulator_dst_lib)
    lipo_dst_lib = lib_path + '/lib{}.a'.format(project_name)

    if not utils.lipo_libs(lipo_src_libs, lipo_dst_lib):
        return False

    if os.path.isfile(libtool_os_dst_lib):
        os.remove(libtool_os_dst_lib)
    if os.path.isfile(libtool_simulator_dst_lib):
        os.remove(libtool_simulator_dst_lib)

    print('==================Output========================')
    print(lipo_dst_lib)
    return True

#不需要依赖列表，需要flutter打包 
def dependencies_info_mapper():
    # info_mapper_path = os.path.join(POD_PROJECT_PATH, 'info_mapper.json')
    return True #utils.load_json(info_mapper_path)

def flutter_build_debug():
    print('flutter_build_debug')
    os.chdir(utils.root_path())
    return os.system('flutter build ios --debug --no-codesign')

def flutter_build_ios():
    print('flutter_build_ios')
    os.chdir(utils.root_path())
    return os.system('flutter build ios-framework --cocoapods --xcframework --no-universal --output=Pods/Flutter/')
    return os.system('flutter build ios-framework --xcframework --no-universal')

def flutter_build_release():
    print('flutter_build_release')
    os.chdir(utils.root_path())
    return os.system('flutter build ios --release --no-codesign')

def flutter_build_sh():
    print('flutter_build_sh')
    os.chdir(utils.root_path())
    os.system('ls')
    return os.system('sh ci-build.sh');

def dependency_info(name, version, infos, is_daily_build):
    info = infos.get(name, {
        'name': name,
        'version': version
    }).copy()

    if version and not is_daily_build:
        info['version'] = info['versions'].get(version, version)
    return info

# 不需要pod
# def pod_content(dependencies_info, project_name, is_daily_build):
#     dependencies = []
#     #便利依赖列表
#     # info_mapper = dependencies_info_mapper()
#     # for n, v in dependencies_info.items():
#         # info = dependency_info(n, v, info_mapper, is_daily_build)
#         # print('name : {}, version : {}'.format(info['name'], info['version']))
#         # dependencies.append("pod '{}', '~> {}'".format(info['name'], info['version']))

#     return '''
# source 'https://github.com/CocoaPods/Specs.git'
# source 'https://git.duowan.com/ci_team/Specs.git'

# target '{project_name}' do
#   platform :ios, '9.0'
#   {dependencies}
# end
# '''.format(project_name=project_name, dependencies='\n  '.join(dependencies))


def generate_pod_dependencies(project_info, output_dir, is_daily_build):
    dependencies_info = project_info['dependencies']
    project_name = project_info['name']
    # print('Runner')
    downloader_project = 'Runner'
    #写入 Podfile
    podfile_path = os.path.join(POD_PROJECT_PATH, 'Podfile')
    # utils.write_content(pod_content(dependencies_info, downloader_project, is_daily_build), podfile_path)
    # print('Runner')
    #用来写framework
    # podfile_path = os.path.join(output_dir, 'Podfile')
    # utils.write_content(pod_content(dependencies_info, project_name, is_daily_build), podfile_path)


def find_headers_path(dependency_name):
    public_headers = os.path.join(POD_PROJECT_PATH, 'Pods/Headers/Public/{}'.format(dependency_name))
    if os.path.isdir(public_headers):
        return public_headers
    proj_path = os.path.join(POD_PROJECT_PATH, 'Pods/{}'.format(dependency_name))
    include_path = os.path.join(proj_path, 'include')
    if os.path.isdir(include_path):
        return include_path
    include_paths = glob.glob('{}/libs/*.framework/Headers'.format(proj_path))
    if len(include_paths) > 0:
        return include_paths[0]


def download_dependencies(project_info, output_dir, is_daily_build, force_update=False):
    dependencies_info = project_info['dependencies']
    # generate_pod_dependencies(project_info, output_dir, is_daily_build)
    print('POD_PROJECT_PATH')
    # print(os.chdir(POD_PROJECT_PATH))
    os.system('ls')
    print('POD_PROJECT')
    # ret = 0
    # if force_update:
    #     ret = os.system('pod install --repo-update')
    # else:
    #     ret = os.system('pod install')
    flutter_build_ios()
    print('flutter_build_ios success')
    # flutter_build_sh()

    # os.chdir(utils.root_path())
    # if ret != 0:
    #     print('!!!!!!!!!!!download dependencies fail!!!!!!!!!!!!!!!')
    #     return False

    # info_mapper = dependencies_info_mapper()
    # if os.path.exists(DEPENDENCY_PATH):
    #     shutil.rmtree(DEPENDENCY_PATH)
    # os.mkdir(DEPENDENCY_PATH)

    # for n in dependencies_info:
        # info = dependency_info(n, '', info_mapper, is_daily_build)
        # origin_path = find_headers_path(info['name'])
        # if not origin_path:
            # continue

        # target_path = os.path.join(DEPENDENCY_PATH, n)
        # os.mkdir(target_path)
        # target_path = os.path.join(target_path, 'include')
        #遍历依赖库
        # header_dir = info.get('header_dir', '')
        # if len(header_dir) > 0:
        #     os.mkdir(target_path)
        #     target_path = os.path.join(target_path, header_dir)
        # os.symlink(origin_path, target_path)
    return True

# 生产plist
def generate_plist(output_dir, project_name, version):
    content = '''
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>CFBundleShortVersionString</key>
	<string>{version}</string>
	<key>CFBundleVersion</key>
	<string>1</string>
</dict>
</plist>
'''.format(version=version)

    plist_path = '{}/{}-Info.plist'.format(output_dir, project_name)
    utils.write_content(content, plist_path)


def generate_zip(output_dir, project_name):
    product_dir = os.path.join(output_dir, project_name)
    shutil.make_archive(product_dir, 'zip', FRAMEWORK_PATH)
    # shutil.move(install_path(project_name), product_dir)


def pack(cfg, project_name, version):
    generate_plist(cfg.output_dir, project_name, version)
    generate_zip(cfg.output_dir, project_name)


def build_version(cfg, project):
    if cfg.action != 'build' or 'version' not in project:
        return cfg.version
    version = project['version']
    if not version.endswith('-dev'):
        version += '-dev'
    return version


def run(cfg):
    print('--------ios---------')
    project = utils.project_info()
    # print(project)
    # print(cfg)

    flutter_build_sh()

    if not os.path.exists(cfg.output_dir):
        os.makedirs(cfg.output_dir)

    project_name = project['name']
    pack(cfg, project_name, build_version(cfg, project))
    # shutil.copy(FRAMEWORK_PATH,cfg.output_dir)
    # os.system('ls ' + cfg.output_dir)
    # force_update_deps = ('UpdatePod' in cfg.macro_arg)
    # is_daily_build = (cfg.action == 'build')
    
    # if download_dependencies(project, cfg.output_dir, is_daily_build, force_update_deps or not is_daily_build) and build(project_name):
        # pack(cfg, project_name, build_version(cfg, project))
        # os.system('ls ' + cfg.output_dir)
        # print('build success')
        # exit(0)
    # exit(1)

