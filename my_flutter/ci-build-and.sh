if [ -z $out ]; then
    out='and_aar'
fi

echo "准备输出所有文件到目录: $out"

echo "清除所有已编译文件"
find . -d -name build | xargs rm -rf
flutter clean
rm -rf $out
rm -rf build

flutter packages get

addFlag(){
    cat .ios/Podfile > tmp1.txt
    echo "and_aar!" >> tmp2.txt
    cat tmp1.txt >> tmp2.txt
    cat tmp2.txt > .android/gradlew
    rm tmp1.txt tmp2.txt
}

# echo "检查 .ios/Podfile文件状态"
# a=$(cat .ios/Podfile)
# if [[ $a == use* ]]; then
#     echo '已经添加use_frameworks, 不再添加'
# else
#     echo '未添加use_frameworks,准备添加'
#     addFlag
#     echo "添加use_frameworks 完成"
# fi

echo "编译flutter"
flutter build aar  
# flutter build apk
echo "编译flutter完成"
mkdir $out
cp -r build/host/outputs/* $out
#release下放开下一行注释，注释掉上一行代码
#cp -r build/ios/Release-iphoneos/*/*.framework $out
# cp -r .ios/Flutter/App.framework $out
# cp -r .ios/Flutter/engine/Flutter.framework $out

echo "复制aar库到临时文件夹: $out"

libpath='../'

rm -rf "$libpath/and_aar"
mkdir $libpath
cp -r $out $libpath

echo "复制库文件到: $libpath"
