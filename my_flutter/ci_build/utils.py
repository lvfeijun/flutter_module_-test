#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import shutil
import glob
import json
import subprocess


def root_path():
    return os.path.abspath(os.path.join(__file__, '../..'))


def project_info():
    project_file = os.path.join(root_path(), 'project.json')
    return load_json(project_file)


def load_json(path):
    with open(path) as json_file:
        return json.load(json_file)


def write_content(content, file_path):
    with open(file_path, 'wb') as f:
        f.write(content.encode('utf-8'))
        f.flush()


def libtool_libs(src_libs, dst_lib):
    src_lib_str = ''
    for l in src_libs:
        src_lib_str = '%s %s'%(src_lib_str, l)

    print(src_lib_str)
    ret = os.system('libtool -static -o %s %s' %(dst_lib, src_lib_str))
    if ret != 0:
        print(('!!!!!!!!!!!libtool %s fail!!!!!!!!!!!!!!!' %(dst_lib)))
        return False

    return True


def lipo_libs(src_libs, dst_lib):
    src_lib_str = ''
    for l in src_libs:
        src_lib_str = '%s %s'%(src_lib_str, l)

    cmd = 'lipo -create %s -output %s' %(src_lib_str, dst_lib)
    ret = os.system(cmd)
    if ret != 0:
        print(('!!!!!!!!!!!lipo_libs %s fail, cmd:%s!!!!!!!!!!!!!!!' %(dst_lib, cmd)))
        return False

    return True


def lipo_thin_libs(src_lib, dst_lib, archs):
    tmp_results = []
    for arch in archs:
        if len(archs) == 1:
            tmp_result = dst_lib
        else:
            tmp_result = dst_lib + '.' + arch

        cmd = 'lipo %s -thin %s -output %s' %(src_lib, arch, tmp_result)
        ret = os.system(cmd)
        if ret != 0:
            print(('!!!!!!!!!!!lipo_thin_libs %s fail, cmd:%s!!!!!!!!!!!!!!!' %(tmp_result, cmd)))
            return False
        tmp_results.append(tmp_result)

    if len(archs) == 1:
        return True
    else:
        return lipo_libs(tmp_results, dst_lib)


def remove_cmake_files(path):
    cmake_files = path + '/CMakeFiles'
    if os.path.exists(cmake_files):
        shutil.rmtree(cmake_files)

    make_files = path + '/Makefile'
    if os.path.isfile(make_files):
        os.remove(make_files)

    cmake_cache = path + '/CMakeCache.txt'
    if os.path.isfile(cmake_cache):
        os.remove(cmake_cache)

    for f in glob.glob(path + '/*.a'):
        os.remove(f)
    for f in glob.glob(path + '/*.so'):
        os.remove(f)


def clean(path, incremental=False):
    if not incremental:
        for fpath, dirs, fs in os.walk(path):
            remove_cmake_files(fpath)

    if not os.path.exists(path):
        os.makedirs(path)


def get_git_revision_hash(project_path=None, is_short=False):
    cmds = ['git']
    if project_path:
        cmds.extend(['-C', project_path])
    cmds.append('rev-parse')
    if is_short:
        cmds.append('--short')
    cmds.append('HEAD')
    return subprocess.check_output(cmds).decode('utf-8').strip()
