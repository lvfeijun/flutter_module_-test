#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
from . import utils


def version_header_content(project_info, file_name):
    _name = project_info.get('name')
    _version_name = _name.upper()
    _version = project_info.get('version', '1.0.0-dev')
    _build_num = os.environ.get('BUILD_NUMBER', 1)
    _commit_id = utils.get_git_revision_hash(project_path=utils.root_path(), is_short=True)
    return '''#ifndef {file_name}_h
#define {file_name}_h
#define {version_name}_VERSION "{version}"
#define {version_name}_BUILD_NUM {build_num}
#define {version_name}_COMMIT_ID "{commit_id}"
#endif /* {file_name}_h */
'''.format(file_name=file_name, version_name=_version_name, version=_version, build_num=_build_num, commit_id=_commit_id)


def generate_version_header(version=None):
    project_info = utils.project_info()
    if version:
        project_info['version'] = version

    file_name = '{}_version'.format(project_info.get('name'))
    content = version_header_content(project_info, file_name)
    file_path = os.path.join(utils.root_path(), 'src/{}.h'.format(file_name))
    utils.write_content(content, file_path)


if __name__ == '__main__':
    version = sys.argv[1] if len(sys.argv) > 1 else None
    generate_version_header(version=version)
