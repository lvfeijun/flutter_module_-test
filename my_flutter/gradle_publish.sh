apply plugin: 'maven-publish'
apply plugin: 'signing'

def isReleaseBuild() {
    return version.contains("SNAPSHOT") == false
}

def getReleaseRepositoryUrl() {
    return hasProperty('RELEASE_REPOSITORY_URL') ? RELEASE_REPOSITORY_URL
            : "https://oss.sonatype.org/service/local/staging/deploy/maven2/"
}

def getSnapshotRepositoryUrl() {
    return hasProperty('SNAPSHOT_REPOSITORY_URL') ? SNAPSHOT_REPOSITORY_URL
            : "https://oss.sonatype.org/content/repositories/snapshots/"
}

def getRepositoryUsername() {
    return hasProperty('NEXUS_USERNAME') ? NEXUS_USERNAME : ""
}

def getRepositoryPassword() {
    return hasProperty('NEXUS_PASSWORD') ? NEXUS_PASSWORD : ""
}

group = GROUP  

task androidJavadocs(type: Javadoc) {
	failOnError false
	source = android.sourceSets.main.java.srcDirs
	classpath += project.files(android.getBootClasspath().join(File.pathSeparator))
	exclude '**/*.so'
}

task androidJavadocsJar(type: Jar, dependsOn: androidJavadocs) {
	classifier = 'javadoc'
	from androidJavadocs.destinationDir
}

task androidSourcesJar(type: Jar) {
	classifier = 'sources'
	from android.sourceSets.main.java.sourceFiles
}

task androidNativeJar(type: Jar) {
	classifier = 'so'
	from(new File(buildDir, 'libs'))
	include("**/*.so")
}

task androidNativeZip(type: Zip) {
	classifier = 'so'
	from(new File(buildDir, 'libs'))
	include("**/*.so")
}


android.libraryVariants

publishing {
    publications {
	  maven(MavenPublication) {
            artifact bundleReleaseAar
		    artifact androidJavadocsJar
    }
    }
}

publishing {
    repositories {
        maven {
	    credentials {
		username = getRepositoryUsername()
		password = getRepositoryPassword()
	    }

	    if(isReleaseBuild()) {
	    url getReleaseRepositoryUrl()
	  } else {
	    url getSnapshotRepositoryUrl()
	  }
        }
    }
}





