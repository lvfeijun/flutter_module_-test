import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_flutter/res/resources.dart';
import 'package:my_flutter/util/theme_utils.dart';
import 'package:my_flutter/widgets/load_image.dart';

/// 空界面:图片+文字  暂无与一一对应的文字和图片数组
/// design/9暂无状态页面/index.html#artboard3
class StateLayout extends StatelessWidget {

  const StateLayout({
    Key? key,
    required this.type,
    this.hintText
  }):super(key: key);

  final StateType type;
  final String? hintText;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        if (type == StateType.loading)
          const CupertinoActivityIndicator(radius: 16.0)
        else
          if (type != StateType.empty)
            Opacity(
              opacity: context.isDark ? 0.5 : 1,
              child: LoadAssetImage(
                'state/${type.img}',///取图片
                width: 120,
              ),
            ),
        const SizedBox(width: double.infinity, height: Dimens.gap_dp16,),
        Text(
          hintText ?? type.hintText,///取文字
          style: Theme.of(context).textTheme.subtitle2?.copyWith(fontSize: Dimens.font_sp14),
        ),
        Gaps.vGap50,
      ],
    );
  }
}

///类型
enum StateType {
  /// 订单
  order,
  /// 商品
  goods,
  /// 无网络
  network,
  /// 消息
  message,
  /// 无提现账号
  account,
  /// 加载中
  loading,
  /// 空
  empty
}

///一一对应
extension StateTypeExtension on StateType {
  String get img => <String>[
    'zwdd', 'zwsp',
    'zwwl', 'zwxx',
    'zwzh', '', '']
  [index];

  String get hintText => <String>[
    '暂无订单', '暂无商品',
    '无网络连接', '暂无消息',
    '马上添加提现账号吧', '', ''
  ][index];
}