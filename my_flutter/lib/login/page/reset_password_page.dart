import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_gen/gen_l10n/deer_localizations.dart';
import 'package:my_flutter/util/change_notifier_manage.dart';
import 'package:my_flutter/res/resources.dart';
import 'package:my_flutter/res/styles.dart';
import 'package:my_flutter/util/toast_utils.dart';
import 'package:my_flutter/util/other_utils.dart';
import 'package:my_flutter/widgets/my_app_bar.dart';
import 'package:my_flutter/widgets/my_button.dart';
import 'package:my_flutter/widgets/my_scroll_view.dart';
import 'package:my_flutter/login/widgets/my_text_field.dart';

import 'package:flutter_boost/flutter_boost.dart';

/// design/1注册登录/index.html#artboard9
class ResetPasswordPage extends StatefulWidget {

  const ResetPasswordPage({Key? key}) : super(key: key);

  @override
  _ResetPasswordPageState createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> with
    ChangeNotifierMixin<ResetPasswordPage> {
  //定义一个controller
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _vCodeController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FocusNode _nodeText1 = FocusNode();
  final FocusNode _nodeText2 = FocusNode();
  final FocusNode _nodeText3 = FocusNode();
  bool _clickable = false;

  @override
  Map<ChangeNotifier, List<VoidCallback>?>? changeNotifier() {
    final List<VoidCallback> callbacks = <VoidCallback>[_verify];
    return <ChangeNotifier, List<VoidCallback>?>{
      _nameController: callbacks,
      _vCodeController: callbacks,
      _passwordController: callbacks,
      _nodeText1: null,
      _nodeText2: null,
      _nodeText3: null,
    };
  }

  void _verify() {
    final String name = _nameController.text;
    final String vCode = _vCodeController.text;
    final String password = _passwordController.text;
    bool clickable = true;
    if (name.isEmpty || name.length < 11) {
      clickable = false;
    }
    if (vCode.isEmpty || vCode.length < 6) {
      clickable = false;
    }
    if (password.isEmpty || password.length < 6) {
      clickable = false;
    }
    if (clickable != _clickable) {
      setState(() {
        _clickable = clickable;
      });
    }
  }

  void _reset() {
    // Toast.show('确定');
    if(_nameController.text.length != 11){
      Toast.show('输入11位的手机号');
      return ;
    } else if(_vCodeController.text.length < 4 ){
      Toast.show('输入正确的验证码');
      return ;
    } else if(_passwordController.text.length < 6 ){
      Toast.show('输入6位以上的密码');
      return ;
    }
    BoostChannel.instance.sendEventToNative('resetpsw',
        {'phone':_nameController.text,'code':_vCodeController.text,'password':_passwordController.text});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        title: '忘记密码',
      ),
      body: MyScrollView(
        keyboardConfig: Utils.getKeyboardActionsConfig(context, <FocusNode>[_nodeText1, _nodeText2, _nodeText3]),
        crossAxisAlignment: CrossAxisAlignment.center,
        padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 20.0),
        children: _buildBody(),
      ),
    );
  }

  List<Widget> _buildBody() {
    return <Widget>[
      Text(
        '忘记密码',
        style: TextStyles.textBold26,
      ),
      Gaps.vGap16,
      MyTextField(
        focusNode: _nodeText1,
        controller: _nameController,
        maxLength: 11,
        keyboardType: TextInputType.phone,
        hintText: '输入手机号',
      ),
      Gaps.vGap8,
      MyTextField(
        focusNode: _nodeText2,
        controller: _vCodeController,
        keyboardType: TextInputType.number,
        getVCode: () async{
          if(_nameController.text.length == 11){
            BoostChannel.instance.sendEventToNative('smsResetpsw',
                {'phone':_nameController.text,'code':_vCodeController.text});
            return true;
          } else {
            Toast.show('输入手机号');
            return false;
          }
        },
        maxLength: 6,
        hintText:'输入验证码',
      ),
      Gaps.vGap8,
      MyTextField(
        focusNode: _nodeText3,
        isInputPwd: true,
        controller: _passwordController,
        maxLength: 16,
        keyboardType: TextInputType.visiblePassword,
        hintText: '输入密码',
      ),
      Gaps.vGap24,
      MyButton(
        onPressed: _clickable ? _reset : null,
        text: '确认',
      )
    ];
  }
}
