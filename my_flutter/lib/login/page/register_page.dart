import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_gen/gen_l10n/deer_localizations.dart';
import 'package:my_flutter/util/change_notifier_manage.dart';
import 'package:my_flutter/res/resources.dart';
import 'package:my_flutter/res/styles.dart';
import 'package:my_flutter/util/toast_utils.dart';
import 'package:my_flutter/util/other_utils.dart';
import 'package:my_flutter/widgets/my_app_bar.dart';
import 'package:my_flutter/widgets/my_button.dart';
import 'package:my_flutter/widgets/my_scroll_view.dart';
import 'package:my_flutter/login/widgets/my_text_field.dart';

import 'package:flutter_boost/flutter_boost.dart';
import 'package:flutter/gestures.dart';

/// design/1注册登录/index.html#artboard11
class RegisterPage extends StatefulWidget {

  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> with ChangeNotifierMixin<RegisterPage> {
  //定义一个controller
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _vCodeController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FocusNode _nodeText1 = FocusNode();
  final FocusNode _nodeText2 = FocusNode();
  final FocusNode _nodeText3 = FocusNode();
  bool _clickable = false;

  @override
  Map<ChangeNotifier, List<VoidCallback>?>? changeNotifier() {
    final List<VoidCallback> callbacks = <VoidCallback>[_verify];
    return <ChangeNotifier, List<VoidCallback>?>{
      _nameController: callbacks,
      _vCodeController: callbacks,
      _passwordController: callbacks,
      _nodeText1: null,
      _nodeText2: null,
      _nodeText3: null,
    };
  }

  void _verify() {
    final String name = _nameController.text;
    final String vCode = _vCodeController.text;
    final String password = _passwordController.text;
    bool clickable = true;
    if (name.isEmpty || name.length < 11) {
      clickable = false;
    }
    if (vCode.isEmpty || vCode.length < 6) {
      clickable = false;
    }
    if (password.isEmpty || password.length < 6) {
      clickable = false;
    }
    if (clickable != _clickable) {
      setState(() {
        _clickable = clickable;
      });
    }
  }

  void _register() {
    if(_nameController.text.length != 11){
      Toast.show('输入11位的手机号');
      return ;
    } else if(_vCodeController.text.length < 4 ){
      Toast.show('输入正确的验证码');
      return ;
    }
    BoostChannel.instance.sendEventToNative('smsLogin',
        {'phone':_nameController.text,'code':_vCodeController.text});
    // Toast.show('点击注册');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        title: '注册',
      ),
      body: MyScrollView(
        keyboardConfig: Utils.getKeyboardActionsConfig(context, <FocusNode>[_nodeText1, _nodeText2, _nodeText3]),
        crossAxisAlignment: CrossAxisAlignment.center,
        padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 20.0),
        children: _buildBody(),
      ),
    );
  }

  List<Widget> _buildBody() {
    return <Widget>[
      Text(
        '注册',
        style: TextStyles.textBold26,
      ),
      Gaps.vGap16,
      MyTextField(
        key: const Key('phone'),
        focusNode: _nodeText1,
        controller: _nameController,
        maxLength: 11,
        keyboardType: TextInputType.phone,
        hintText: '输入手机号',
      ),
      Gaps.vGap8,
      MyTextField(
        key: const Key('vcode'),
        focusNode: _nodeText2,
        controller: _vCodeController,
        keyboardType: TextInputType.number,
        getVCode: () async {
          if (_nameController.text.length == 11) {
            // Toast.show('确认');
            /// 一般可以在这里发送真正的请求，请求成功返回true
            BoostChannel.instance.sendEventToNative('sms',
                {'phone':_nameController.text});
            return true;
          } else {
            Toast.show('输入手机号');
            return false;
          }
        },
        maxLength: 6,
        hintText: '输入验证码',
      ),
      Gaps.vGap8,
      MyTextField(
        key: const Key('password'),
        keyName: 'password',
        focusNode: _nodeText3,
        isInputPwd: true,
        controller: _passwordController,
        keyboardType: TextInputType.visiblePassword,
        maxLength: 16,
        hintText: '输入密码',
      ),
      Gaps.vGap24,
      Container(
        alignment: Alignment.centerLeft,
        child: RichText(
          text: TextSpan(
            text: '同意',
            style: Theme.of(context).textTheme.subtitle2?.copyWith(fontSize: Dimens.font_sp14),
            children: <TextSpan>[
              TextSpan(
                text: '《用户协议》',
                style: TextStyle(
                  color: Colors.blue,
                ),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    BoostNavigator.instance.push("webview", arguments:{
                      'title': '《用户协议》',
                      'url':'https://www.hqwx.com/aboutus/policy/user.html',
                    }, withContainer: false);
                  },
              ),
              TextSpan(text: '和'),
              TextSpan(
                text: '《隐私协议》',
                style: TextStyle(
                  color: Colors.blue,
                ),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    BoostNavigator.instance.push("webview", arguments:{
                      'title': '《隐私协议》',
                      'url':'https://www.hqwx.com/aboutus/policy/hqwx.html',
                    }, withContainer: false);
                  },
              ),
            ],
          ),
        ),
      ),
      Gaps.vGap24,
      MyButton(
        key: const Key('register'),
        onPressed: _clickable ? _register : null,
        text:  '注册',
      )
    ];
  }
}
