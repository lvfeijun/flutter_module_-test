import 'dart:ui';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_gen/gen_l10n/deer_localizations.dart';
import 'package:my_flutter/util/change_notifier_manage.dart';
import 'package:my_flutter/res/resources.dart';
import 'package:my_flutter/res/styles.dart';
import 'package:my_flutter/routers/fluro_navigator.dart';
import 'package:my_flutter/util/toast_utils.dart';
import 'package:my_flutter/util/other_utils.dart';
import 'package:my_flutter/widgets/my_app_bar.dart';
import 'package:my_flutter/widgets/my_button.dart';
import 'package:my_flutter/widgets/my_scroll_view.dart';
import 'package:my_flutter/login/widgets/my_text_field.dart';

import '../login_router.dart';
import 'package:flutter_boost/flutter_boost.dart';

/// design/1注册登录/index.html#artboard4
class SMSLoginPage extends StatefulWidget {
  const SMSLoginPage({Key? key}) : super(key: key);

  @override
  _SMSLoginPageState createState() => _SMSLoginPageState();
}

class _SMSLoginPageState extends State<SMSLoginPage> with ChangeNotifierMixin<SMSLoginPage> {

  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _vCodeController = TextEditingController();
  final FocusNode _nodeText1 = FocusNode();
  final FocusNode _nodeText2 = FocusNode();
  bool _clickable = false;

  @override
  Map<ChangeNotifier, List<VoidCallback>?>? changeNotifier() {
    final List<VoidCallback> callbacks = <VoidCallback>[_verify];
    return <ChangeNotifier, List<VoidCallback>?>{
      _phoneController: callbacks,
      _vCodeController: callbacks,
      _nodeText1: null,
      _nodeText2: null,
    };
  }

  void _verify() {
    final String name = _phoneController.text;
    final String vCode = _vCodeController.text;
    bool clickable = true;
    if (name.isEmpty || name.length < 11) {
      clickable = false;
    }
    if (vCode.isEmpty || vCode.length < 6) {
      clickable = false;
    }
    if (clickable != _clickable) {
      setState(() {
        _clickable = clickable;
      });
    }
  }

  void _login() {
    if(_phoneController.text.length != 11){
      Toast.show('输入11位的手机号');
      return ;
    } else if(_vCodeController.text.length < 4 ){
      Toast.show('输入正确的验证码');
      return ;
    }
    BoostChannel.instance.sendEventToNative('smsLogin',
        {'phone':_phoneController.text,'code':_vCodeController.text});
    // Toast.show('去登录......');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MyAppBar(),
      body: MyScrollView(
        keyboardConfig: Utils.getKeyboardActionsConfig(context, <FocusNode>[_nodeText1, _nodeText2]),
        padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 20.0),
        children: _buildBody(),
      ),
    );
  }

  List<Widget> _buildBody() {
    return <Widget>[
      Text(
        '输入验证码',
        style: TextStyles.textBold26,
      ),
      Gaps.vGap16,
      MyTextField(
        focusNode: _nodeText1,
        controller: _phoneController,
        maxLength: 11,
        keyboardType: TextInputType.phone,
        hintText: '输入验证码',
      ),
      Gaps.vGap8,
      MyTextField(
        focusNode: _nodeText2,
        controller: _vCodeController,
        maxLength: 6,
        keyboardType: TextInputType.number,
        hintText: '输入验证码',
        getVCode: () async {
          if (_phoneController.text.length == 11) {
            BoostChannel.instance.sendEventToNative('sms',
                {'phone':_phoneController.text});
            return true;
          }else{
            Toast.show('输入手机号');
            return false;
          }
        },
      ),
      Gaps.vGap24,
      Container(
        alignment: Alignment.centerLeft,
        child: RichText(
          text: TextSpan(
            text: '同意',
            style: Theme.of(context).textTheme.subtitle2?.copyWith(fontSize: Dimens.font_sp14),
            children: <TextSpan>[
              TextSpan(
                text: '《用户协议》',
                style: TextStyle(
                  color: Colors.blue,
                ),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    BoostNavigator.instance.push("webview", arguments:{
                      'title': '《用户协议》',
                      'url':'https://www.hqwx.com/aboutus/policy/user.html',
                    }, withContainer: false);
                  },
              ),
              TextSpan(text: '和'),
              TextSpan(
                text: '《隐私协议》',
                style: TextStyle(
                  color: Colors.blue,
                ),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    BoostNavigator.instance.push("webview", arguments:{
                      'title': '《隐私协议》',
                      'url':'https://www.hqwx.com/aboutus/policy/hqwx.html',
                    }, withContainer: false);
                  },
              ),
            ],
          ),
        ),
      ),
      Gaps.vGap24,
      MyButton(
        onPressed: _clickable ? _login : null,
        text: '登录',
      ),
      Container(
        height: 40.0,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            // Gaps.vGap8,
            Container(
              alignment: Alignment.centerLeft,
              child: RichText(
                text: TextSpan(
                  text: '点击',
                  style: Theme.of(context).textTheme.subtitle2?.copyWith(fontSize: Dimens.font_sp14),
                  children: <TextSpan>[
                    TextSpan(
                      text: '注册',
                      style: TextStyle(
                        color: Colors.blue,
                      ),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          BoostNavigator.instance.push("register", withContainer: false);
                          // NavigatorUtils.push(context, LoginRouter.registerPage);
                        },
                    ), // TextSpan(text: Utils.getCurrLocale() == 'zh' ? '。' : '.',),
                  ],
                ),
              ),
            ),
            Container(
              alignment: Alignment.centerRight,
              child: GestureDetector(
                child: Text(
                  '忘记密码',
                  style: Theme.of(context).textTheme.subtitle2,
                ),
                onTap: () => BoostNavigator.instance.push("resetPassword", withContainer: false),
                // NavigatorUtils.push(context, LoginRouter.resetPasswordPage),
              ),
            ),
          ],
        )
      )
    ];
  }
}
