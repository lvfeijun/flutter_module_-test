import 'package:my_flutter/mvp/base_page_presenter.dart';
import 'package:my_flutter/net/net.dart';
import 'package:my_flutter/login/models/login_entity.dart';
import 'package:my_flutter/login/iview/login_page_iview.dart';
import 'package:my_flutter/util/toast_utils.dart';
import 'package:my_flutter/widgets/state_layout.dart';

class LoginPageProvider extends BasePagePresenter<LoginPageIMvpView> {

  Future search(String phone, String password, bool isShowDialog) {

    final Map<String, String> params = <String, String>{};
    params['phone'] = phone;
    params['password'] = password;
    return requestNetwork<LoginEntity>(Method.post,
        url: HttpApi.login,
        queryParameters: params,
        isShow: isShowDialog,
        onSuccess: (data) {
          if (data != null && data.items != null) {
          } else {
            Toast.show('加载失败');
          }
        },
        onError: (int code, String msg) {
          Toast.show('${code}:${msg}');
        }
    );
  }

}