import 'package:my_flutter/mvp/mvps.dart';
import 'package:my_flutter/login/models/login_entity.dart';
import 'package:my_flutter/login/provider/login_page_provider.dart';

abstract class LoginPageIMvpView implements IMvpView {

  LoginPageProvider get provider;
}