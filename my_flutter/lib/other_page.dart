import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_boost/flutter_boost.dart';

class OtherPage extends StatefulWidget {
  OtherPage({this.data=''});
  final String data;

  @override
  _SplashPageState createState() => _SplashPageState(data:data);
}

class _SplashPageState extends State<OtherPage> {
  _SplashPageState({this.data=''});
  final String data;

  @override
  void initState() {
    super.initState();
    Logger.log('boost-SplashPage $mounted');
  }

  @override
  Widget build(BuildContext context) {
    Logger.log('boost-SplashPage $mounted');
    return Scaffold(
      appBar: AppBar(
        title:  Text("Flutter 页面"),
      ),
      body:Container(child:Center(
        child: Flex(
          direction: Axis.vertical,
          children: [
            GestureDetector(
              onTap: (){
                BoostNavigator.instance.pop("native");
              },
              child: Text(
                  "这是另外的源生页面"
              ),
            ),
            Text(data),
          ],
        ),
      ),
      ),
    );
  }
}
