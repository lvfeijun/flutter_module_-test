
#!/bin/bash

function ini_android() {
    echo "[INFO] init android flutter env"
    flutter precache --android --no-ios
    echo "[INFO] init android flutter env end"
}

function ini_ios() {
    echo "[INFO] init ios flutter env"
    flutter precache --ios --no-android
    echo "[INFO] init ios flutter env end"
}

if [ -z $1 ];then
    ini_ios
else
    ini_$1

fi